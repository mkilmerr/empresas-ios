//
//  ServiceConstants.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import Foundation

//MARK:- URL Constantes

struct ServiceConstants {
    static let API_VERSION = "v1"
    static let ROOT_URL = "https://empresas.ioasys.com.br/api/\(API_VERSION)"
    static let SIGN_IN_URL = "\(ROOT_URL)/users/auth/sign_in"
    static let ENTERPRISES_URL = "\(ROOT_URL)/enterprises"
    static let UPLOADS_URL = "https://empresas.ioasys.com.br"
}
