//
//  LoginService.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import Foundation

//MARK:- Classe destinada aos serviço de SignIn

class SignInService: NSObject {
    private let signInUrl = ServiceConstants.SIGN_IN_URL
    private let userCustomHeaders = UserCustomHeadersSingleton.shared
    weak var loadingProtocol:LoadingProtocol?
    
    func signIn(email:String, password:String, completion: @escaping(User?, Error?) -> Void) {
        guard let url = URL(string: signInUrl) else { return }
        var request = URLRequest(url: url)
        
        self.loadingProtocol?.start()
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        
        do {
            let params = [
                "email" : email,
                "password" : password
            ]
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .init())
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if error != nil {
                    completion(nil,error)
                    return
                    
                }
               
                if let httpResponse = response as? HTTPURLResponse {
                    if let uid = httpResponse.allHeaderFields["uid"] as? String, let client = httpResponse.allHeaderFields["client"] as? String, let accessToken = httpResponse.allHeaderFields["access-token"] as? String {
                        
                        self.userCustomHeaders.setCustomHeaders(udid:uid,client:client, accessToken:accessToken)
                        
                    }
                }
                
                if let statusResponse = response as? HTTPURLResponse {
                    if let status = statusResponse.allHeaderFields["Status"] as? String {
                        self.userCustomHeaders.setStatusCode(statusCode: status)
                    }
                }
                
                guard let investorData = data else { return }
                self.loadingProtocol?.stop()
                do {
                    let investor = try JSONDecoder().decode(User.self, from: investorData)
                    
                    completion(investor,nil)
                    return
                    
                } catch {
                    completion(nil,error)
                    return
                }
                
                
            }.resume()
            
        } catch {
           
        }
        
        
        
    }
}
