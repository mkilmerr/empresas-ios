//
//  EnterpriseService.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

import Foundation

//MARK:- Classe destinada aos serviços relacionados a busca de empresas

class EnterpriseService: NSObject {
    private let enterprisesUrl = ServiceConstants.ENTERPRISES_URL
    weak var loadingProtocol:LoadingProtocol?
    
    func getAllEnterprises(completion: @escaping([Enterprise]?, Error?) -> Void) {
        
        guard let url = URL(string: enterprisesUrl) else { return }
        var request = URLRequest(url: url)
        
        self.loadingProtocol?.start()
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(UserCustomHeadersSingleton.shared.accessToken, forHTTPHeaderField: "access-token")
        request.addValue(UserCustomHeadersSingleton.shared.client, forHTTPHeaderField: "client")
        request.addValue(UserCustomHeadersSingleton.shared.udid, forHTTPHeaderField: "uid")
        
        request.httpMethod = "GET"
        
        do {
        
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if error != nil {
                    completion(nil,error)
                    return

                }
                
                guard let enterpriseData = data else { return }
                self.loadingProtocol?.stop()
                
                do {
                    let json = try JSONSerialization.jsonObject(with: enterpriseData, options: []) as? [String : Any]
                    
                    let enterprisesJson = json?["enterprises"]
                    
                    let jsonData = try JSONSerialization.data(withJSONObject: enterprisesJson as Any, options: .prettyPrinted)
                  
                    
                    let enterprises = try JSONDecoder().decode([Enterprise].self, from: jsonData)
                   
                    
                    completion(enterprises,nil)
                    return
                    
                } catch {
                    completion(nil,error)
                    return
                }
            }.resume()
        } 
        
    }
}
