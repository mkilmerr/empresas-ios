//
//  LoadingAnimation.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

import UIKit
import Lottie

//MARK:- Classe destinada a animação de carregamento

class LoadingAnimation {
    
    static var loadingAnimationView: AnimationView = {
        let animationView = AnimationView()
        animationView.contentMode = .scaleAspectFit
        animationView.animation = Animation.named("loading_animation")
        animationView.backgroundColor = .white
        animationView.translatesAutoresizingMaskIntoConstraints = false
        return animationView
    }()
    
    class func startLoading(_ mainView:UIView) {
        
        mainView.addSubview(self.loadingAnimationView)
        
        NSLayoutConstraint.activate([
            self.loadingAnimationView.centerXAnchor.constraint(equalTo: mainView.centerXAnchor),
            self.loadingAnimationView.centerYAnchor.constraint(equalTo: mainView.centerYAnchor),
            self.loadingAnimationView.widthAnchor.constraint(equalTo: mainView.widthAnchor),
            self.loadingAnimationView.heightAnchor.constraint(equalTo: mainView.heightAnchor)
        ])
        self.loadingAnimationView.alpha = 0.3
        self.loadingAnimationView.play()
        mainView.layoutIfNeeded()
        
        
    }
    
    class func stopLoading() {
        
        UIView.animate(withDuration: 0.5) {
            self.loadingAnimationView.stop()
            self.loadingAnimationView.removeFromSuperview()
            NSLayoutConstraint.deactivate(self.loadingAnimationView.constraints)
        }
        
        
        
    }
}
