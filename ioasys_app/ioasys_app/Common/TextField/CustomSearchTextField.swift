//
//  CustomSearchTextField.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

import UIKit

//MARK:- TextField de pesquisa customizado

class CustomSearchTextField: UITextField {
    
    lazy var searchCompanyIcon: UIImageView = { [unowned self] in
        let imageView = UIImageView()
        imageView.image = UIImage.searchCompany
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = UIColor.commonGray
        self.setupSearchBarProperties()
        self.setupSearchCompanyIcon()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSearchCompanyIcon() {
        self.addSubview(self.searchCompanyIcon)
        
        NSLayoutConstraint.activate([
            self.searchCompanyIcon.widthAnchor.constraint(equalToConstant: 20),
            self.searchCompanyIcon.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)

        override open func textRect(forBounds bounds: CGRect) -> CGRect {
            return bounds.inset(by: padding)
        }

        override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
            return bounds.inset(by: padding)
        }

        override open func editingRect(forBounds bounds: CGRect) -> CGRect {
            return bounds.inset(by: padding)
        }
    
    func setupSearchBarProperties() {
        self.layer.borderWidth = 1.2
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColor.commonGray.cgColor
        self.placeholder = "Pesquise por empresa"
        
        self.leftView = self.searchCompanyIcon
        self.leftViewMode = .always
    }
}
