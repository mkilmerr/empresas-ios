//
//  CustomFloatingTextField.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import UIKit

//MARK:- TextField customizado com label flutuante

class CustomFloatingTextField:UITextField{
    var floatingPlaceHolder:String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = UIColor.commonGray
        self.setupTextFieldProperties()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupTextFieldProperties() {
        self.layer.borderWidth = 1.2
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColor.commonGray.cgColor
    }
    
    func setupFloatingPlaceHolder(_ view:UIView) {
        let floatingLabel = UILabel()
        floatingLabel.textColor = UIColor.black
        floatingLabel.font = UIFont.systemFont(ofSize: 15)
        floatingLabel.translatesAutoresizingMaskIntoConstraints = false
        floatingLabel.text = self.floatingPlaceHolder ?? ""
        
        view.addSubview(floatingLabel)
        NSLayoutConstraint.activate([
            floatingLabel.bottomAnchor.constraint(equalTo: self.topAnchor, constant: -10),
            floatingLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor)
        ])
    }
}



