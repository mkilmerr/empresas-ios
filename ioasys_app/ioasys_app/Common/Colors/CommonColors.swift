//
//  CommonColors.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import UIKit

//MARK:- Cores usadas no projeto

extension UIColor {
    static let commonLightPurple = UIColor(red: 188/255, green: 65/255, blue: 132/255, alpha: 0.9)
    static let commonPurple = UIColor(red: 173/255, green: 31/255, blue: 140/255, alpha: 1.0)
    static let commonLightRose = UIColor(red: 173/255, green: 128/255, blue: 168/255, alpha: 1.0)
    static let commonGray = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0)
    static let commonSignInButtonColor = UIColor(red: 224/255, green: 30/255, blue: 105/255, alpha: 1.0)
}
