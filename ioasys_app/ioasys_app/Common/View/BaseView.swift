//
//  BaseView.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import UIKit

class BaseView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints(_ main:UIView) {}
    
}
