//
//  CommonImages.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import UIKit

//MARK:- Imagens do projeto

extension UIImage {
    static let logoImage = UIImage(named: "logo_home")
    static let seePasswordImage = UIImage(named: "see_password")
    static let wrongCredentials = UIImage(named: "wrong_credentials")
    static let homeHeader = UIImage(named: "home_header")
    static let searchCompany = UIImage(named: "search_company")
    static let backButton = UIImage(named: "back_button")
    static let homeEnterprise = UIImage(named: "home_enterprise")
    static let sharePrice = UIImage(named: "share_price")
    static let enterpriseType = UIImage(named: "enterprise_type")
    
}
