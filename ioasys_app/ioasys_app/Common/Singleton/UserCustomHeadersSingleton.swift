//
//  UserSingleton.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

//MARK:- Singleton destinado ao preenchimento do custom header

import Foundation

class UserCustomHeadersSingleton {
    static let shared = UserCustomHeadersSingleton()
    var udid:String = ""
    var client:String = ""
    var accessToken:String = ""
    var statusCode:String = ""
    
    
    func setCustomHeaders(udid:String,client:String,accessToken:String) {
        self.udid = udid
        self.client = client
        self.accessToken = accessToken
       
    }
    
    func setStatusCode(statusCode:String) {
        let onlyNumberStatus = statusCode.split{$0 == " "}.map(String.init)[0]
        self.statusCode = onlyNumberStatus
    }
    
}
