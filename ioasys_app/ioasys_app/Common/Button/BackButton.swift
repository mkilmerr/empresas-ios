//
//  BackButton.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 02/01/21.
//

import UIKit

//MARK:- Botão customizado para a função de sair da tela

class BackButton: UIView {
    
    lazy var arrowImage:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.backButton
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false 
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.commonGray
        self.layer.cornerRadius = 4
        translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(arrowImage)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupContraints(_ mainView:UIView) {
        mainView.addSubview(self)
        self.addSubview(arrowImage)
        NSLayoutConstraint.activate([
            self.widthAnchor.constraint(equalToConstant: 40),
            self.heightAnchor.constraint(equalToConstant: 40),
            self.topAnchor.constraint(equalTo: mainView.safeAreaLayoutGuide.topAnchor),
            self.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 16)
        ])
        
        
        NSLayoutConstraint.activate([
            self.arrowImage.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.arrowImage.centerYAnchor.constraint(equalTo:self.centerYAnchor),
            self.arrowImage.widthAnchor.constraint(equalToConstant:7),
            self.arrowImage.heightAnchor.constraint(equalToConstant: 13)
            
        ])
        
        
    }
}
