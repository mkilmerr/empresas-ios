//
//  NetworkResponse.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

//MARK:- Enum destinado ao status code das requisições

import Foundation

enum NetworkResponse:String {
    case ok = "200"
    case unauthorized = "401"
    
    var responseType: NetworkResponse {
        
        switch self.rawValue {
        case "200":
            return .ok
        case "401":
            return .unauthorized
        default:
            return .unauthorized
            
        }
        
    }
}
