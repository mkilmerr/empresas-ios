//
//  SignInViewModel.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

import Foundation

//MARK:- ViewModel destinada ao processo de SignIn

public class SignInViewModel {
    let signInService = SignInService()
    var userCredentialsIsWrong = false
    var user:User?
    
    var password:String = ""
    var email:String = ""
    
    var loadingProtocolObserver:SignInViewController?{
        didSet {
            self.signInService.loadingProtocol = loadingProtocolObserver
        }
    }
    public init(email:String, password:String) {
        self.email = email
        self.password = password
    }
   
    public func callSignService(handler: @escaping(User?, Error?) -> Void) {
        self.signInService.signIn(email: self.email, password: self.password) { (user, error) in
            
            if error != nil {
                handler(nil,error)
            }
             
            let networkResponse = NetworkResponse.init(rawValue: UserCustomHeadersSingleton.shared.statusCode)
            
            guard let networkResponseType = networkResponse else { return }
            self.handleStatusCode(statusCode: networkResponseType)
            
            handler(user,nil)
        }
    }
}

extension SignInViewModel {
    func handleStatusCode(statusCode:NetworkResponse) {
        
        switch statusCode {
        case .ok:
            self.userCredentialsIsWrong = false
        case .unauthorized:
            self.userCredentialsIsWrong = true
            
        }
    }
}

extension SignInViewModel {
    
    public func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self.email)
    }
    
    public func isValidPassword() -> Bool {
        if self.password.count >= 4 {
            return true
        } else {
            return false
        }
    }
}

