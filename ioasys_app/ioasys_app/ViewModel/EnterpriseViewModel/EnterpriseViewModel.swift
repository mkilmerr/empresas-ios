//
//  EnterpriseViewModel.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 02/01/21.
//

import Foundation

//MARK:- ViewModel destinada as informações da empresa

class EnterpriseViewModel {
    let enterpriseService = EnterpriseService()
    var enterprises = [Enterprise]()
    
    var loadingProtocolObserver:HomeViewController?{
        didSet {
            self.enterpriseService.loadingProtocol = loadingProtocolObserver
        }
    }
    
    func callGetAllEnterprises(handler: @escaping([Enterprise]?, Error?) -> Void) {
        self.enterpriseService.getAllEnterprises { (enterprises, error) in
            
            if error != nil {
                handler(nil,error)
            }
            if let enterprisesUnwrapped = enterprises {
                self.enterprises = enterprisesUnwrapped
            }
            handler(enterprises,nil)
        }
    }
}
