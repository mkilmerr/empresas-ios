//
//  EnterpriseCollectionViewController.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 02/01/21.
//

import UIKit

//MARK:- Classe destinada a tela de Detalhes de cada empresa 
class EnterpriseViewController: UIViewController {
    
    let enterpriseView = EnterpriseView()
    let enterpriseInfoCollectionView = EnterpriseInfoCollectionView()
    
    var enterprise:Enterprise? {
        didSet {
            if let name = enterprise?.enterpriseName, let photoUrl = enterprise?.photo {
                let photo =  ServiceConstants.UPLOADS_URL + photoUrl
                
                self.enterpriseView.enterpriseImage.sd_setImage(with: URL(string: photo), completed: nil)
                
                DispatchQueue.main.async {
                    self.enterpriseView.enterpriseNameLabel.text = name
                }
                
            }
            
            guard let description = enterprise?.enterpriseDescription  else { return }
            DispatchQueue.main.async {
                self.enterpriseView.descriptionLabel.text = description
            }
           
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        self.enterpriseView.backButtonProtocol = self
        
        self.enterpriseView.setupConstraints(self.view)
        
        self.enterpriseInfoCollectionView.setupConstraints(self)
        self.enterpriseInfoCollectionView.delegate = self
        self.enterpriseInfoCollectionView.dataSource = self
        
        self.enterpriseView.setupDescriptionTitleLabel(self)
        
    }
    
}

extension EnterpriseViewController:BackButtonProtocol {
    func backButtonDidTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension EnterpriseViewController:UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EnterpriseInfoCollectionViewCell.cellIdentifier, for: indexPath) as! EnterpriseInfoCollectionViewCell
        
        cell.enterprise = self.enterprise
        cell.setupCell(row: indexPath.row)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width * 0.8, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(30)
    }
    
}

