//
//  SignInViewController.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import UIKit

//MARK:- Classe destinada a controller da tela de SignIn
class SignInViewController: UIViewController {
    
    
    let signInView = SignInView()
    var signInViewModel: SignInViewModel?
    

    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.signInView.signInButtonProtocol = self
        self.view.backgroundColor = .white
        self.signInView.setupConstraints(self.view)

        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
}

extension SignInViewController {
    func showAlertWhenTextIsEmpty() {
        let alert = UIAlertController(title: "Calma lá!", message: "Ops...Acho que você esqueceu de preencher alguns campos", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension SignInViewController:SignInButtonProtocol {
    
    
    func signButtonTappedWhenTextIsEmpty() {
        self.showAlertWhenTextIsEmpty()
    }
    
    func signButtonTapped(email: String, password: String) {
        self.signInViewModel = SignInViewModel(email:email, password:password)
        self.signInViewModel?.loadingProtocolObserver = self
        
        self.signInViewModel?.callSignService() { (user, error) in
            
            DispatchQueue.main.async {
                
            if user?.investor != nil {
                self.signInView.cleanWrongCredentials()
                self.presentToHomeViewController()
            } else {
                self.signInView.setupWrongCredentials()
            }
            }
            
        }
        
        
        
    }
    
    func presentToHomeViewController() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
            let homeViewController = HomeViewController()
            homeViewController.modalPresentationStyle = .fullScreen
            self.present(homeViewController, animated: true, completion: nil)
        }
        
    }
    
}

extension SignInViewController:LoadingProtocol {
    func start() {
        DispatchQueue.main.async {
            LoadingAnimation.startLoading(self.view)
        }
       
    }
    
    func stop() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            LoadingAnimation.stopLoading()
        }
        
    }
    
    
}
