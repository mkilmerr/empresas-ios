//
//  HomeViewController.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

import UIKit

//MARK:- Classe destinada a controller da tela de Home

class HomeViewController: UIViewController {
    
    let headerHome = HeaderHome()
    let enterpriseCollectionView = EnterpriseCollectionView()
    let enterpriseService = EnterpriseService()
    let enterpriseViewModel = EnterpriseViewModel()
    var enterpriseCopyData = [Enterprise]()
    
    
    var enterprisesData:[Enterprise]? {
        didSet {
            DispatchQueue.main.async {
                self.enterpriseCollectionView.reloadData()
            }
        }
    }
    
    
    override func viewDidLoad() {
        self.view.backgroundColor = .white
        
        self.headerHome.searchEnterpriseProtocol = self
        self.headerHome.setupConstraints(self.view)
        self.enterpriseCollectionView.setupConstraints(self)
        self.enterpriseCollectionView.delegate = self
        self.enterpriseCollectionView.dataSource = self
        
        self.enterpriseViewModel.loadingProtocolObserver = self
        
        self.enterpriseViewModel.callGetAllEnterprises { (enterprises, error) in
            self.enterpriseCopyData = enterprises ?? [Enterprise]()
            self.enterprisesData = enterprises
            self.setResultsFind(amount: enterprises?.count ?? 0)
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
}

extension HomeViewController:SearchEnterpriseProtocol {
    func searchEnterprise(search: String) {
        self.filterCollectionView(search)
    }
    
    
    func filterCollectionView(_ search:String) {

        let filter = self.enterprisesData?.filter({

            $0.enterpriseName.hasPrefix(search) 
        })
       
        
        if search.isEmpty || search.count == 1  || search == ""{
            self.enterprisesData = self.enterpriseCopyData
            self.setResultsFind(amount: self.enterpriseViewModel.enterprises.count)
            
            self.enterpriseCollectionView.reloadData()
            
        } else {
            self.enterprisesData = filter
            self.setResultsFind(amount: filter?.count ?? 0)
            
            self.enterpriseCollectionView.reloadData()
        }
    }
    
    func setResultsFind(amount:Int) {
        DispatchQueue.main.async {
            if amount == 0 {
                self.enterpriseCollectionView.setupNoResultsLabel()
                self.headerHome.amountResultsLabel.isHidden = true
            } else {
                self.headerHome.amountResultsLabel.isHidden = false
                self.enterpriseCollectionView.removeNoResultsLabel()
                self.headerHome.amountResultsLabel.text = "\(String(describing: amount)) resultados encontrados"
            }
           
        }
    }
}




extension HomeViewController:LoadingProtocol {
    func start() {
        DispatchQueue.main.async {
            LoadingAnimation.startLoading(self.view)
        }
       
    }
    
    func stop() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            LoadingAnimation.stopLoading()
        }
        
    }
    
    
}




extension HomeViewController:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.enterprisesData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EnterpriseCollectionViewCell.cellIdentifier, for: indexPath) as! EnterpriseCollectionViewCell
        
        cell.enterprise = self.enterprisesData?[indexPath.row]
        
        return cell 
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let enterpriseViewController = EnterpriseViewController()
        enterpriseViewController.modalPresentationStyle = .fullScreen
        enterpriseViewController.modalTransitionStyle  = .coverVertical
        enterpriseViewController.enterprise = self.enterprisesData?[indexPath.row]
        self.present(enterpriseViewController, animated: true, completion: nil)
       
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(40)
    }
    
}
