//
//  HeaderHome.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

import UIKit

//MARK:- Classe destinada ao header da HomeViewController

class HeaderHome: UIView {
    
    weak var searchEnterpriseProtocol:SearchEnterpriseProtocol?
    var heightConstraint: NSLayoutConstraint?
    var headerHeight = CGFloat(250)
    
    lazy var headerBackgroundImage:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.homeHeader
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false 
        return imageView
    }()
    
    lazy var searchTextField:CustomSearchTextField = { [unowned self] in
        let textfield = CustomSearchTextField()
        textfield.delegate = self
        return textfield
    }()
    
    lazy var amountResultsLabel:UILabel = {
       let label = UILabel()
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        
        self.heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.headerHeight)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension HeaderHome:BaseView {
    func setupConstraints(_ mainView: UIView) {
        mainView.addSubview(self)
        self.addSubview(self.headerBackgroundImage)
        mainView.addSubview(self.searchTextField)
        mainView.addSubview(self.amountResultsLabel)
        
        NSLayoutConstraint.activate([
            self.widthAnchor.constraint(equalTo: mainView.widthAnchor),
            self.topAnchor.constraint(equalTo: mainView.topAnchor),
            self.centerXAnchor.constraint(equalTo: mainView.centerXAnchor)
        ])
        
        self.heightConstraint?.constant = 200
        self.addConstraint(self.heightConstraint!)
        
        NSLayoutConstraint.activate([
            self.headerBackgroundImage.widthAnchor.constraint(equalTo: self.widthAnchor),
            self.headerBackgroundImage.heightAnchor.constraint(equalTo: self.heightAnchor)
        ])
        
        NSLayoutConstraint.activate([
            self.searchTextField.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9),
            self.searchTextField.heightAnchor.constraint(equalToConstant: 56),
            self.searchTextField.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.searchTextField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 40)
        ])
        
        NSLayoutConstraint.activate([
            self.amountResultsLabel.topAnchor.constraint(equalTo: self.searchTextField.bottomAnchor, constant: 10),
            self.amountResultsLabel.leadingAnchor.constraint(equalTo: self.searchTextField.leadingAnchor)
        ])
        
        self.setupAccessibilityIdentifier()
    }
    
}

extension HeaderHome:UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.heightConstraint?.constant = 100
        UIView.animate(withDuration: 0.5) {
            self.layoutIfNeeded()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.searchEnterpriseProtocol?.searchEnterprise(search: textField.text ?? "")
        
        self.heightConstraint?.constant = 100
        UIView.animate(withDuration: 0.5) {
            self.layoutIfNeeded()
        }
        
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.heightConstraint?.constant = 250
        UIView.animate(withDuration: 0.5) {
            self.layoutIfNeeded()
        }
        
        textField.resignFirstResponder();
        return true;
    }
}

extension HeaderHome {
    func setupAccessibilityIdentifier() {
        
        self.searchTextField.isAccessibilityElement = true
        self.searchTextField.accessibilityIdentifier = "searchTextFieldIdentifier"
        

    }
}
