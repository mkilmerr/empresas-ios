//
//  Card.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import UIKit

//MARK:- Classe destinada ao header da SignInView

class HeaderSignInView : UIView {
    
    let path = UIBezierPath()
    
    private var heightConstraint: NSLayoutConstraint?
    private var headerHeight = CGFloat(250)
    
    
    lazy var mainTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Seja bem vindo(a) ao empresas!"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var logoImage:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.logoImage
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        
        self.heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.headerHeight)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func draw(_ rect: CGRect) {
        self.drawHeaderView()
        self.setGradientBackground(path:path)
        
    }
}

extension HeaderSignInView {
    
}
// add to main view and setup constraints
extension HeaderSignInView:BaseView {
    func setupConstraints(_ mainView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        mainView.addSubview(self)
        
        NSLayoutConstraint.activate([
            self.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: 1.0),
        ])
        self.heightConstraint?.constant = 250
        self.addConstraint(self.heightConstraint!)
        setupHeaderViewWhenUserIsNotTyping(mainView)
    }
    
   
    
}

extension HeaderSignInView {
    func setupHeaderViewWhenUserIsNotTyping(_ mainView:UIView) {
        mainView.addSubview(self.mainTitleLabel)
        mainView.addSubview(self.logoImage)
        mainView.layer.addSublayer(self.logoImage.layer)
        mainView.layer.addSublayer(self.mainTitleLabel.layer)
       
        
        NSLayoutConstraint.activate([
            
            self.logoImage.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.logoImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.logoImage.widthAnchor.constraint(equalToConstant: 40),
            self.logoImage.heightAnchor.constraint(equalToConstant: 32)
            
        ])
      
        NSLayoutConstraint.activate([

            self.mainTitleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.mainTitleLabel.topAnchor.constraint(equalTo: self.logoImage.bottomAnchor, constant: 16)
        ])
        
    }
    
    func setupHeaderViewWhenUserIsTyping(_ mainView:UIView) {
        self.heightConstraint?.constant = 100
        self.mainTitleLabel.isHidden = true
        UIView.animate(withDuration: 0.5) {
            mainView.layoutIfNeeded()
            self.layoutIfNeeded()
        }
    }
    
    
}

// set background gradient color
extension HeaderSignInView {
    func setGradientBackground(path:UIBezierPath) {
        let shape = CAShapeLayer()
        shape.path = path.cgPath
        shape.lineWidth = 2.0
        
        self.layer.addSublayer(shape)
        
        let gradient = CAGradientLayer()
        gradient.frame = path.bounds
        gradient.colors = [UIColor.commonPurple.cgColor, UIColor.commonLightPurple.cgColor, UIColor.commonLightRose.cgColor]
        
        let shapeMask = CAShapeLayer()
        shapeMask.path = path.cgPath
        gradient.mask = shapeMask
        
        self.layer.addSublayer(gradient)
        
    }
}

// draw the view
extension HeaderSignInView {
    func drawHeaderView() {
        let cardRadius = CGFloat(30)
        
        let buttonSlotRadius = CGFloat(0)
        
        let viewSize = self.bounds.size
        
        let effectiveViewHeight = viewSize.height - buttonSlotRadius
        
        
        
        path.move(to: CGPoint(x: cardRadius, y: 0))
        
        
        path.addLine(to: CGPoint(x: viewSize.width - cardRadius, y: 0))
        
        path.addArc(
            withCenter: CGPoint(
                x: viewSize.width - cardRadius,
                y: cardRadius
            ),
            radius: cardRadius,
            startAngle: CGFloat(Double.pi * 3 / 2),
            endAngle: CGFloat(0),
            clockwise: true
        )
        
        path.addLine(
            to: CGPoint(x: viewSize.width, y: effectiveViewHeight)
        )
        
        path.addArc(
            withCenter: CGPoint(
                x: viewSize.width - cardRadius,
                y: effectiveViewHeight - cardRadius
            ),
            radius: cardRadius,
            startAngle: CGFloat(0),
            endAngle: CGFloat(Double.pi / 2),
            clockwise: true
        )
        
        path.addLine(
            to: CGPoint(x: viewSize.width / 4 * 3, y: effectiveViewHeight)
        )
        
        path.addArc(
            withCenter: CGPoint(
                x: viewSize.width / 4 * 3 - buttonSlotRadius,
                y: effectiveViewHeight
            ),
            radius: buttonSlotRadius,
            startAngle: CGFloat(0),
            endAngle: CGFloat(Double.pi / 2),
            clockwise: true
        )
        
        
        path.addLine(
            to: CGPoint(
                x: viewSize.width / 4 + buttonSlotRadius,
                y: effectiveViewHeight + buttonSlotRadius
            )
        )
        
        path.addArc(
            withCenter: CGPoint(
                x: viewSize.width / 4 + buttonSlotRadius,
                y: effectiveViewHeight
            ),
            radius: buttonSlotRadius,
            startAngle: CGFloat(Double.pi / 2),
            endAngle: CGFloat(Double.pi),
            clockwise: true
        )
        
        path.addLine(
            to: CGPoint(x: cardRadius, y: effectiveViewHeight)
        )
        
        path.addArc(
            withCenter: CGPoint(
                x: cardRadius,
                y: effectiveViewHeight - cardRadius
            ),
            radius: cardRadius,
            startAngle: CGFloat(Double.pi / 2),
            endAngle: CGFloat(Double.pi),
            clockwise: true
        )
        
        path.addLine(to: CGPoint(x: 0, y: cardRadius))
        
        path.addArc(
            withCenter: CGPoint(x: cardRadius, y: cardRadius),
            radius: cardRadius,
            startAngle: CGFloat(Double.pi),
            endAngle: CGFloat(Double.pi / 2 * 3),
            clockwise: true
        )
        
        
        
        path.close()
        
        path.fill()

    }
}
