//
//  HeaderView.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import UIKit

class HeaderView: UIView {
    
    let path = UIBezierPath()
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .yellow
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
// setup constraints to HeaderView
extension HeaderView {
    func setupConstraints(_ mainView:UIView) {
        mainView.addSubview(self)
        
        NSLayoutConstraint.activate([
            self.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: 1.0),
            self.heightAnchor.constraint(equalTo: mainView.heightAnchor, multiplier: 0.2)
        ])
        
    let shapeLayer = CAShapeLayer()
    let path = UIBezierPath(rect: frame)

    shapeLayer.path = path.cgPath
    layer.insertSublayer(shapeLayer, at: 0)
        
//        addDownwardCurve(to: self.path, to: CGPoint(x: 10, y: ), withAmplitude: 12.0)
              
    }
    
    func addDownwardCurve(to path: UIBezierPath?, to point: CGPoint, withAmplitude amplitude: CGFloat) {
    let mid = (point.x + (path?.currentPoint.x ?? 0.0)) / 2
    path?.addQuadCurve(to: point, controlPoint: CGPoint(x: mid, y: point.y + amplitude))
    }
}

//let view = UIView(frame: CGRect(x: 50, y: 100, width: 200, height: 300))
//
//       let path = UIBezierPath()
//       path.move(to: CGPoint(x: 0.0, y: 200))
//       path.addCurve(to: CGPoint(x: 200, y:150),
//                     controlPoint1: CGPoint(x: 50, y: 350),
//                     controlPoint2: CGPoint(x:150, y: 0))
//       path.addLine(to: CGPoint(x: view.frame.size.width, y: view.frame.size.height))
//       path.addLine(to: CGPoint(x: 0.0, y: view.frame.size.height))
//       path.close()
//
//       let shapeLayer = CAShapeLayer()
//       shapeLayer.path = path.cgPath
//
//       view.backgroundColor = UIColor.black
//       view.layer.mask = shapeLayer
//       self.view.addSubview(view)
