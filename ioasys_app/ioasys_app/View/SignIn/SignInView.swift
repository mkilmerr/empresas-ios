//
//  SignInView.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import UIKit
import Lottie

//MARK:- Classe destinada a view da SignInViewController

class SignInView:UIView {
    let headerView = HeaderSignInView()
    weak var signInButtonProtocol: SignInButtonProtocol?
    
    var tapGestureRecognizerSeePassword : UITapGestureRecognizer?
    var tapGestureRecognizerWrongCredentials:UITapGestureRecognizer?
    var tapGestureRecognizerWrongEmailCredential:UITapGestureRecognizer?
    
    var seePasswordClick = true
    
    
    lazy var logoImage:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.logoImage
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var mainTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Seja bem vindo(a) ao empresas!"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var emailTextField: CustomFloatingTextField = { [unowned self] in
        let textfield = CustomFloatingTextField()
        textfield.floatingPlaceHolder = "Email"
        textfield.delegate = self
        textfield.autocapitalizationType = .none
        textfield.rightViewMode = .always
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
    
    lazy var passwordTextField: CustomFloatingTextField = { [unowned self] in
        
        let textfield = CustomFloatingTextField()
        textfield.floatingPlaceHolder = "Senha"
        textfield.isSecureTextEntry = true
        textfield.rightViewMode = .always
        textfield.delegate = self
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
    
    lazy var seePasswordImage: UIImageView = { [unowned self] in
        let imageView = UIImageView()
        imageView.image = UIImage.seePasswordImage
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(self.tapGestureRecognizerSeePassword!)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var wrongCredentials: UIImageView = { [unowned self] in
        let imageView = UIImageView()
        imageView.image = UIImage.wrongCredentials
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(self.tapGestureRecognizerWrongCredentials!)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var wrongEmailCredential: UIImageView = { [unowned self] in
        let imageView = UIImageView()
        imageView.image = UIImage.wrongCredentials
        imageView.isUserInteractionEnabled = true
                imageView.addGestureRecognizer(self.tapGestureRecognizerWrongEmailCredential!)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var signInButton: UIButton = { [unowned self] in
        let button = UIButton(type: .system)
        button.setTitle("ENTRAR", for: .normal)
        button.backgroundColor = UIColor.commonSignInButtonColor
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 8
        button.addTarget(self, action: #selector(self.signInButtonDidTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var wrongCredentialsLabel:UILabel = {
       let label = UILabel()
        label.text = "Credenciais incorretas"
        label.textColor = .red
        label.font = UIFont.systemFont(ofSize: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        
       
        
        self.tapGestureRecognizerSeePassword = UITapGestureRecognizer(target: self, action: #selector(self.seePasswordDidTapped))
        
        self.tapGestureRecognizerWrongCredentials = UITapGestureRecognizer(target: self, action: #selector(self.wrongCredentialsDidTapped))
        
        self.tapGestureRecognizerWrongEmailCredential = UITapGestureRecognizer(target: self, action: #selector(self.wrongEmailCredentialDidTapped))
        
        
        self.passwordTextField.rightView = self.seePasswordImage
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SignInView:BaseView {
    func setupConstraints(_ mainView: UIView) {
      
        self.headerView.setupConstraints(mainView)
        mainView.addSubview(self)
        self.addSubview(self.emailTextField)
        self.addSubview(self.passwordTextField)
        self.addSubview(self.seePasswordImage)
        self.addSubview(self.wrongCredentials)
        self.addSubview(self.wrongEmailCredential)
        self.addSubview(self.signInButton)
        
        self.wrongCredentials.isHidden = true
        self.wrongEmailCredential.isHidden = true
        
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: 0),
            self.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: 1.0),
            self.heightAnchor.constraint(equalTo: mainView.heightAnchor, multiplier: 0.7)
        ])
        
        
        NSLayoutConstraint.activate([
            self.emailTextField.topAnchor.constraint(equalTo: self.topAnchor, constant: 60),
            self.emailTextField.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9),
            self.emailTextField.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.emailTextField.heightAnchor.constraint(equalToConstant: 48)
        ])
        
        emailTextField.setupFloatingPlaceHolder(self)
        
        NSLayoutConstraint.activate([
            self.passwordTextField.topAnchor.constraint(equalTo: self.emailTextField.bottomAnchor, constant: 50),
            self.passwordTextField.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9),
            self.passwordTextField.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.passwordTextField.heightAnchor.constraint(equalToConstant: 48)
        ])
        
        passwordTextField.setupFloatingPlaceHolder(self)
        
        NSLayoutConstraint.activate([
            self.seePasswordImage.widthAnchor.constraint(equalToConstant: 22),
            self.seePasswordImage.heightAnchor.constraint(equalToConstant: 15)
        ])
        
        
        NSLayoutConstraint.activate([
            self.wrongCredentials.widthAnchor.constraint(equalToConstant: 20),
            self.wrongCredentials.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        NSLayoutConstraint.activate([
            self.wrongEmailCredential.widthAnchor.constraint(equalToConstant: 20),
            self.wrongEmailCredential.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        
        
        NSLayoutConstraint.activate([
            self.signInButton.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: 50),
            self.signInButton.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9),
            self.signInButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.signInButton.heightAnchor.constraint(equalToConstant: 48)
        ])
        
        self.setupAccessibilityIdentifier()
    }
    
}

extension SignInView:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.headerView.setupHeaderViewWhenUserIsTyping(self)
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
}

extension SignInView {
    @objc func seePasswordDidTapped() {
        if(self.seePasswordClick == true) {
            self.passwordTextField.isSecureTextEntry = false
        } else {
            self.passwordTextField.isSecureTextEntry = true
        }
        
        self.seePasswordClick = !self.seePasswordClick
    }
    
    @objc func wrongCredentialsDidTapped() {
        DispatchQueue.main.async {
            self.passwordTextField.text = ""
        }
    }
    
    @objc func wrongEmailCredentialDidTapped() {
        DispatchQueue.main.async {
            self.emailTextField.text = ""
        }
    }
}

extension SignInView {
    @objc func signInButtonDidTapped() {
        if self.passwordTextField.text == "" || self.emailTextField.text == "" {
            
            self.signInButtonProtocol?.signButtonTappedWhenTextIsEmpty()
            
        } else {
            self.signInButtonProtocol?.signButtonTapped(email: self.emailTextField.text ?? "", password: self.passwordTextField.text ?? "")
        }
        
       
        
    }
}

extension SignInView {
    func setupWrongCredentialsLabel() {
        
        self.emailTextField.layer.borderColor = UIColor.red.cgColor
        self.passwordTextField.layer.borderColor = UIColor.red.cgColor
       
        self.addSubview(wrongCredentialsLabel)
        
        NSLayoutConstraint.activate([
            wrongCredentialsLabel.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: 13),
            wrongCredentialsLabel.trailingAnchor.constraint(equalTo: self.passwordTextField.trailingAnchor)
        ])
    }
    
    func removeWrongCredentialsLabel() {
        self.emailTextField.layer.borderColor = UIColor.commonGray.cgColor
        self.passwordTextField.layer.borderColor = UIColor.commonGray.cgColor
        self.wrongCredentialsLabel.removeFromSuperview()
    }
}

extension SignInView {
    func cleanWrongCredentials() {
        self.passwordTextField.rightView = self.seePasswordImage
        self.wrongCredentials.isHidden = true
        self.wrongEmailCredential.isHidden = true
        
        self.removeWrongCredentialsLabel()
        self.layoutIfNeeded()
        self.reloadInputViews()
    }
    
    func setupWrongCredentials() {
        self.passwordTextField.rightView = self.wrongCredentials
        self.emailTextField.rightView = self.wrongEmailCredential
        self.wrongEmailCredential.isHidden = false
        self.wrongCredentials.isHidden = false
        
        self.setupWrongCredentialsLabel()
        
        self.layoutIfNeeded()
        self.reloadInputViews()
    }
}

extension SignInView {
    func setupAccessibilityIdentifier() {
        emailTextField.isAccessibilityElement = true
        emailTextField.accessibilityIdentifier = "emailTextFieldIdentifier"
        
        passwordTextField.isAccessibilityElement = true
        passwordTextField.accessibilityIdentifier = "passwordTextFieldIdentifier"
        
        signInButton.isAccessibilityElement = true
        signInButton.accessibilityIdentifier = "signInButtonIdentifier"
        
        wrongCredentials.isAccessibilityElement = true
        wrongCredentials.accessibilityIdentifier = "wrongCredentialsIdentifier"
        
        seePasswordImage.isAccessibilityElement = true
        seePasswordImage.accessibilityIdentifier = "seePasswordImageIdentifier"
        
        wrongCredentialsLabel.isAccessibilityElement = true
        wrongCredentialsLabel.accessibilityIdentifier = "wrongCredentialsLabelIdentifier"
       
    }
}
