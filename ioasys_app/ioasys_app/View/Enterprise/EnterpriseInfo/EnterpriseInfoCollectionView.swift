//
//  EnterpriseCollectionView.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 02/01/21.
//

import UIKit

//MARK:- Classe destinada a UICollectionView da HomeViewController

class EnterpriseInfoCollectionView: UICollectionView {
    let layout = UICollectionViewFlowLayout()
    
    init() {
        super.init(frame: .zero, collectionViewLayout: self.layout)
        self.setupEnterpriseInfoCollectionView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupEnterpriseInfoCollectionView() {
       
        self.showsHorizontalScrollIndicator = false
        layout.scrollDirection = .horizontal
        
        self.register(EnterpriseInfoCollectionViewCell.self, forCellWithReuseIdentifier: EnterpriseInfoCollectionViewCell.cellIdentifier)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = .white
    }
    
    func setupConstraints(_ mainViewController:EnterpriseViewController) {
        
        mainViewController.view.addSubview(self)
        
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: mainViewController.enterpriseView.infoTitleLabel.bottomAnchor, constant: 10),
            self.widthAnchor.constraint(equalTo: mainViewController.view.widthAnchor, multiplier: 1.0),
            self.heightAnchor.constraint(equalTo: mainViewController.view.heightAnchor, multiplier: 0.2),
            self.centerXAnchor.constraint(equalTo: mainViewController.view.centerXAnchor)
        ])
        
    }
}
