//
//  EnterpriseView.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 02/01/21.
//

import UIKit

//MARK:- Classe destinada a view da EnterpriseViewController

class EnterpriseView: UIView {
    
    var backButtonTap:UITapGestureRecognizer?
    
    weak var backButtonProtocol: BackButtonProtocol?
    
    lazy var backButton:BackButton = {
        let backButton = BackButton()
        backButton.translatesAutoresizingMaskIntoConstraints = false
        return backButton
    }()
    
    lazy var enterpriseNameLabel:UILabel = {
       let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var enterpriseImage:UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var infoTitleLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.text = "Informações"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var descriptionTitleLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.text = "Descrição"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var descriptionLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.lineBreakMode = .byWordWrapping
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backButtonTap = UITapGestureRecognizer(target: self, action: #selector(self.backButtonDidTapped))
        backButton.addGestureRecognizer(self.backButtonTap ?? UITapGestureRecognizer())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension EnterpriseView {
    @objc func backButtonDidTapped() {
        self.backButtonProtocol?.backButtonDidTapped()
    }
}

extension EnterpriseView {
    func setupConstraints(_ mainView:UIView) {
        mainView.addSubview(self)
        mainView.addSubview(self.enterpriseNameLabel)
        mainView.addSubview(self.enterpriseImage)
        mainView.addSubview(self.infoTitleLabel)
        
        NSLayoutConstraint.activate([
            self.widthAnchor.constraint(equalTo: mainView.widthAnchor),
            self.heightAnchor.constraint(equalTo: mainView.heightAnchor)
        ])
        
        self.backButton.setupContraints(mainView)
        
        NSLayoutConstraint.activate([
            self.enterpriseNameLabel.topAnchor.constraint(equalTo: mainView.safeAreaLayoutGuide.topAnchor),
            self.enterpriseNameLabel.centerXAnchor.constraint(equalTo: mainView.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            self.enterpriseImage.widthAnchor.constraint(equalTo: mainView.widthAnchor),
            self.enterpriseImage.heightAnchor.constraint(equalTo: mainView.heightAnchor, multiplier: 0.2),
            self.enterpriseImage.topAnchor.constraint(equalTo: self.backButton.bottomAnchor, constant: 50)
        ])
        
        NSLayoutConstraint.activate([
            self.infoTitleLabel.topAnchor.constraint(equalTo: enterpriseImage.bottomAnchor, constant: 50),
            self.infoTitleLabel.leadingAnchor.constraint(equalTo: backButton.leadingAnchor)
        ])
       
    }
    
    func setupDescriptionTitleLabel(_ viewController:EnterpriseViewController) {
        
        viewController.view.addSubview(self.descriptionTitleLabel)
        
        NSLayoutConstraint.activate([
            self.descriptionTitleLabel.topAnchor.constraint(equalTo: viewController.enterpriseInfoCollectionView.bottomAnchor, constant: 30),
            self.descriptionTitleLabel.leadingAnchor.constraint(equalTo: backButton.leadingAnchor)
        ])
        
        viewController.view.addSubview(self.descriptionLabel)
        
        NSLayoutConstraint.activate([
            self.descriptionLabel.topAnchor.constraint(equalTo: self.descriptionTitleLabel.bottomAnchor, constant: 20),
            self.descriptionLabel.widthAnchor.constraint(equalTo: viewController.view.widthAnchor, multiplier: 0.9),
            self.descriptionLabel.leadingAnchor.constraint(equalTo: backButton.leadingAnchor),
//            self.descriptionLabel.bottomAnchor.constraint(equalTo: viewController.view.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        
    }
}
