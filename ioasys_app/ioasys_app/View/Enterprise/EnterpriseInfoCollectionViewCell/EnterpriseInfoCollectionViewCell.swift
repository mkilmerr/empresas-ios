//
//  EnterpriseInfoCollectionViewCell.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 02/01/21.
//

import UIKit

//MARK:- Classe destinada a célula da EnterpriseInfoCollectionView

class EnterpriseInfoCollectionViewCell: UICollectionViewCell {
    
    static let cellIdentifier = "EnterpriseInfoCollectionViewCell"
    
    var enterprise:Enterprise? {
        didSet {
            if let city = enterprise?.city, let country = enterprise?.country, let serviceType = enterprise?.enterpriseType.enterpriseTypeName, let sharePrice = enterprise?.sharePrice {
                
                DispatchQueue.main.async {
                    self.countryLabel.text = country
                    self.cityTitleLabel.text = city
                    let service = serviceType
                    self.serviceTypeLabel.text = "Área de atuacão : " + service
                    self.serviceTypeLabel.setSubTextColor(pSubString: service, pColor: UIColor.commonLightPurple)
                    let price = "US$ \(sharePrice)"
                    self.subtitleSharePriceLabel.text = price
                   
                }
               
            }
        }
    }
    
    lazy var cellImage:UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var cityTitleLabel:UILabel = {
       let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var countryLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.commonLightPurple
        label.font = UIFont.systemFont(ofSize: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var serviceTypeLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var sharePriceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.adjustsFontSizeToFitWidth = true
        label.text = "Preço compartilhado"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var subtitleSharePriceLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.commonLightPurple
        label.font = UIFont.systemFont(ofSize: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupViews()
        self.setupConstraints()
        self.layer.cornerRadius = 6
        self.backgroundColor = UIColor.commonGray
       
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension EnterpriseInfoCollectionViewCell {
    
   
    func setupCell(row:Int) {
        switch row {
        case 0:
            self.setupLocation()
        case 1:
            self.setupService()
        default:
            self.setupPrice()
        }
    }
    
    func setupLocation() {
        self.cellImage.image = UIImage.homeEnterprise
        self.addSubview(self.cityTitleLabel)
        self.addSubview(self.countryLabel)
        
        NSLayoutConstraint.activate([
            self.cityTitleLabel.topAnchor.constraint(equalTo: self.cellImage.topAnchor),
            self.cityTitleLabel.centerYAnchor.constraint(equalTo: self.cellImage.centerYAnchor,constant: -5),
            self.cityTitleLabel.leadingAnchor.constraint(equalTo: self.cellImage.trailingAnchor, constant: 10)
        ])
        
        NSLayoutConstraint.activate([
            self.countryLabel.topAnchor.constraint(equalTo: self.cityTitleLabel.bottomAnchor, constant: -14),
            self.countryLabel.leadingAnchor.constraint(equalTo: self.cellImage.trailingAnchor, constant: 10)
        ])
        
    }
    
    func setupService() {
        self.cellImage.image = UIImage.enterpriseType
        self.addSubview(self.serviceTypeLabel)
        NSLayoutConstraint.activate([
            self.serviceTypeLabel.topAnchor.constraint(equalTo: self.cellImage.topAnchor),
            self.serviceTypeLabel.centerYAnchor.constraint(equalTo: self.cellImage.centerYAnchor,constant: -5),
            self.serviceTypeLabel.leadingAnchor.constraint(equalTo: self.cellImage.trailingAnchor, constant: 10)
        ])
    }
    
    func setupPrice() {
        self.cellImage.image = UIImage.sharePrice
        self.addSubview(self.sharePriceLabel)
        self.addSubview(self.subtitleSharePriceLabel)
        
        NSLayoutConstraint.activate([
            self.sharePriceLabel.topAnchor.constraint(equalTo: self.cellImage.topAnchor),
            self.sharePriceLabel.centerYAnchor.constraint(equalTo: self.cellImage.centerYAnchor,constant: -5),
            self.sharePriceLabel.leadingAnchor.constraint(equalTo: self.cellImage.trailingAnchor, constant: 10)
        ])
        
        NSLayoutConstraint.activate([
            self.subtitleSharePriceLabel.topAnchor.constraint(equalTo: self.sharePriceLabel.bottomAnchor, constant: -14),
            self.subtitleSharePriceLabel.leadingAnchor.constraint(equalTo: self.cellImage.trailingAnchor, constant: 10)
        ])
    }
    func setupViews() {
        self.addSubview(self.cellImage)
        
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            self.cellImage.widthAnchor.constraint(equalToConstant: 40),
            self.cellImage.heightAnchor.constraint(equalToConstant: 40),
            self.cellImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.cellImage.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 10),
            self.cellImage.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 16)
        ])
    }
}
