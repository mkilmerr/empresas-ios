//
//  EnterpriseCollectionView.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 02/01/21.
//

import UIKit

//MARK:- Classe destinada a CollectionView da HomeViewController

class EnterpriseCollectionView: UICollectionView {
    let layout = UICollectionViewFlowLayout()
    var mainView:UIView?
    
    lazy var noResultsLabel:UILabel = {
       let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.text = "Nenhum resultado encontrado"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    init() {
        super.init(frame: .zero, collectionViewLayout: self.layout)
        self.setupEnterpriseCollectionView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupEnterpriseCollectionView() {
       
      
        layout.minimumLineSpacing = 40
        layout.scrollDirection = .vertical
        
        self.register(EnterpriseCollectionViewCell.self, forCellWithReuseIdentifier: EnterpriseCollectionViewCell.cellIdentifier)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = .white
    }
    
    func setupConstraints(_ mainViewController:HomeViewController) {
        self.mainView = mainViewController.view
        mainViewController.view.addSubview(self)
        
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: mainViewController.headerHome.searchTextField.bottomAnchor, constant: 50),
            self.widthAnchor.constraint(equalTo: mainViewController.view.widthAnchor, multiplier: 1.0),
            self.heightAnchor.constraint(equalTo: mainViewController.view.heightAnchor, multiplier: 1.0)
        ])
        
    }
    
    func setupNoResultsLabel() {
        guard let view = mainView else { return }
        view.addSubview(self.noResultsLabel)
        self.noResultsLabel.isHidden = false
        
        NSLayoutConstraint.activate([
            self.noResultsLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            self.noResultsLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        self.noResultsLabel.isAccessibilityElement = true
        self.noResultsLabel.accessibilityIdentifier = "noResultsLabelIdentifier"
    }
    
    func removeNoResultsLabel() {
        self.noResultsLabel.isHidden = true
    }
}
