//
//  EnterpriseCollectionViewCell.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 02/01/21.
//

import UIKit
import SDWebImage

//MARK:- Classe destinada a célula da EnterpriseCollectionView

class EnterpriseCollectionViewCell: UICollectionViewCell {
    
    var enterprise:Enterprise? {
        didSet {
            if let photoUrl = enterprise?.photo, let name = enterprise?.enterpriseName {
                
                let photo =  ServiceConstants.UPLOADS_URL + photoUrl
                
                self.backgroundInterpriseImage.sd_setImage(with: URL(string: photo), completed: nil)
                
                self.enterpriseButtonName.setTitle(name, for: .normal)
                
            }
        }
    }
    
    lazy var backgroundInterpriseImage:UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var enterpriseButtonName:UIButton = {
       let button = UIButton()
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.commonLightPurple
        button.isUserInteractionEnabled = false
        button.layer.cornerRadius = 4
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    static let cellIdentifier = "EnterpriseCollectionViewCell"
    override init(frame: CGRect) {
        super.init(frame: frame)
       
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = UIColor.systemBackground.withAlphaComponent(0.1)
        self.contentView.layer.cornerRadius = 4.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        self.setupViews()
        self.setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension EnterpriseCollectionViewCell {
    func setupViews() {
        self.addSubview(self.backgroundInterpriseImage)
        self.addSubview(self.enterpriseButtonName)
    }
    
    func setupConstraints() {

        
        NSLayoutConstraint.activate([
            backgroundInterpriseImage.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            backgroundInterpriseImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            backgroundInterpriseImage.widthAnchor.constraint(equalTo: self.contentView.widthAnchor, multiplier: 0.9),
            backgroundInterpriseImage.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            enterpriseButtonName.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            enterpriseButtonName.centerYAnchor.constraint(equalTo:contentView.centerYAnchor),
            enterpriseButtonName.widthAnchor.constraint(equalToConstant: 280),
            enterpriseButtonName.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
}
