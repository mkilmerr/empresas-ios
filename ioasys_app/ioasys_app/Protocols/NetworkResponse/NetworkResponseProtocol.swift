//
//  NetworkResponseProtocol.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

import Foundation

protocol NetworkResponseProtocol: class {
    func getStatusCode(_ code:String)
}
