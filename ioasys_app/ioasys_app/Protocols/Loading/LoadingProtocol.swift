//
//  LoadingProtocol.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

import Foundation

//MARK:- Protocolo destinado a animação de carregamento

protocol LoadingProtocol: class {
    func start()
    func stop()
}
