//
//  BackButtonProtocol.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 02/01/21.
//

import Foundation

//MARK:- Protocolo para as ações do botão de voltar

protocol BackButtonProtocol: class {
    func backButtonDidTapped()
}
