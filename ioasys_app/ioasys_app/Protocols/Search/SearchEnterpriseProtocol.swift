//
//  SearchEnterpriseProtocol.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 02/01/21.
//

import Foundation

//MARK:- Protocolo destinado a ação de busca

protocol SearchEnterpriseProtocol: class {
    func searchEnterprise(search:String)
}
