//
//  BaseView.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import UIKit

//MARK:- Protocolo implementado pelas Views para montar as constraints

protocol BaseView {
    func setupConstraints(_ mainView:UIView)
}
