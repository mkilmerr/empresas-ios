//
//  SignInButtonProtocol.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

import Foundation

//MARK:- Protocolo destinado as ações do botão de SignIn

protocol SignInButtonProtocol:class {
    func signButtonTapped(email:String, password:String)
    func signButtonTappedWhenTextIsEmpty()
   
}
