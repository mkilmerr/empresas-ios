//
//  PortifolioModel.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import Foundation

//MARK:- Model destinada ao Portifolio da empresa

public struct Portfolio: Decodable {
    public let enterprisesNumber: Int
    public let enterprises: [Enterprise]?

    enum CodingKeys: String, CodingKey {
        case enterprisesNumber = "enterprises_number"
        case enterprises
    }
}
