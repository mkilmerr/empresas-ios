//
//  EnterpriseType.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import Foundation

//MARK:- Model destinada ao Tipo da Empresa

public struct EnterpriseType: Decodable {
    public let id: Int
    public let enterpriseTypeName: String

    enum CodingKeys: String, CodingKey {
        case id
        case enterpriseTypeName = "enterprise_type_name"
    }
}
