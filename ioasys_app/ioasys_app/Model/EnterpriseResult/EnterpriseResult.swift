//
//  EnterPriseResult.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 01/01/21.
//

import Foundation

struct EnterpriseResult: Decodable {
    let enterprises: Enterprise
}
