//
//  EnterpriseModel.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import Foundation

//MARK:- Model destinada a Empresa

public struct Enterprise: Decodable {
    public let id: Int
    public let enterpriseName, enterpriseDescription: String
    public let emailEnterprise, facebook, twitter, linkedin: String?
    public let phone: String?
    public let ownEnterprise: Bool?
    public let photo: String?
    public let value, shares, sharePrice, ownShares: Int?
    public let city, country: String?
    public let enterpriseType: EnterpriseType

    enum CodingKeys: String, CodingKey {
        case id
        case enterpriseName = "enterprise_name"
        case enterpriseDescription = "description"
        case emailEnterprise = "email_enterprise"
        case facebook, twitter, linkedin, phone
        case ownEnterprise = "own_enterprise"
        case photo, value, shares
        case sharePrice = "share_price"
        case ownShares = "own_shares"
        case city, country
        case enterpriseType = "enterprise_type"
    }
}
