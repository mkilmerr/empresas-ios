//
//  UserModel.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import Foundation

//MARK:- Model destinada ao Usuário

public struct User:Decodable {
    let investor:Investor?
    let enterprise: Enterprise?
    let success: Bool
}
