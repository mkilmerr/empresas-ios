//
//  InvestorModel.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 31/12/20.
//

import Foundation

//MARK:- Model destinada a model do Investidor

public struct Investor: Decodable {
    public let id: Int
    public let investorName, email, city, country: String
    public let balance: Int
    public let photo: String?
    public let portfolio: Portfolio?
    public let portfolioValue: Int
    public let firstAccess, superAngel: Bool

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case investorName = "investor_name"
        case email, city, country, balance, photo, portfolio
        case portfolioValue = "portfolio_value"
        case firstAccess = "first_access"
        case superAngel = "super_angel"
    }
}
