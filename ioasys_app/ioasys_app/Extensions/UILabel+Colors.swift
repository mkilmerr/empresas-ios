//
//  UILabel+Colors.swift
//  ioasys_app
//
//  Created by Marcos Kilmer on 02/01/21.
//

import UIKit

//MARK:- Extensão para colocar duas cores em um mesmo Label

extension UILabel{

    func setSubTextColor(pSubString : String, pColor : UIColor){
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: self.text!);
        let range = attributedString.mutableString.range(of: pSubString, options:NSString.CompareOptions.caseInsensitive)
        if range.location != NSNotFound {
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: pColor, range: range);
        }
        self.attributedText = attributedString

    }
}
