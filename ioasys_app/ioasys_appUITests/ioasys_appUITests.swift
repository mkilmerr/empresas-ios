//
//  ioasys_appUITests.swift
//  ioasys_appUITests
//
//  Created by Marcos Kilmer on 03/01/21.
//

import XCTest

//MARK:- Classe destinada aos testes de UI

class ioasys_appUITests: XCTestCase {
    
    let app = XCUIApplication()
    
    lazy var emailTextField:XCUIElement = { [unowned self] in
        let element = self.app.textFields["emailTextFieldIdentifier"]
        return element
    }()
    
    lazy var passwordTextField:XCUIElement = { [unowned self] in
        let element = self.app.secureTextFields["passwordTextFieldIdentifier"]
        return element
    }()
    
    lazy var signInButton:XCUIElement = { [unowned self] in
        let element = self.app.buttons["signInButtonIdentifier"]
        return element
    }()
    
    lazy var seePasswordImage:XCUIElement = { [unowned self] in
        let element = self.app.images["seePasswordImageIdentifier"]
        return element
    }()
    
    lazy var wrongCredentialsLabel:XCUIElement = { [unowned self] in
        let element = self.app.staticTexts["Credenciais incorretas"]
        return element
    }()
    
    lazy var searchTextField:XCUIElement = { [unowned self] in
        let element = self.app.textFields["searchTextFieldIdentifier"]
        return element
    }()
    
    lazy var noResultsLabel:XCUIElement = { [unowned self] in
        let element = self.app.staticTexts["Nenhum resultado encontrado"]
        return element
    }()
    
    override func setUpWithError() throws {
        self.app.launch()
        continueAfterFailure = false
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    //
    func testSignInWithWrongCredentials() {
       
        self.emailTextField.tap()
        self.emailTextField.typeText("wrong@wrong.com.br")
        self.passwordTextField.tap()
        self.passwordTextField.typeText("wrong")
        
        self.signInButton.tap()
        
        XCTAssertTrue(wrongCredentialsLabel.waitForExistence(timeout: 5.0))
        
    }
    
    func testSignInWithCorrectCredentials() {
       
        self.emailTextField.tap()
        self.emailTextField.typeText("testeapple@ioasys.com.br")
        self.passwordTextField.tap()
        self.passwordTextField.typeText("12341234")
        
        self.signInButton.tap()
        
        XCTAssertFalse(wrongCredentialsLabel.waitForExistence(timeout: 5.0))
    }
    
    func testSignInWithIncompleteInfos() {
        
        XCTAssertEqual(self.emailTextField.value as! String, "")
        
        self.signInButton.tap()
        
        let elementsQuery = app.alerts["Calma lá!"].scrollViews.otherElements
        
        let okButton = elementsQuery.buttons["OK"]
        okButton.tap()
        
    }
    
    func testSearchNonExistentEnterprise() {
        
        self.testSignInWithCorrectCredentials()
        
        XCTAssertTrue(self.searchTextField.waitForExistence(timeout: 5.0))
        
        self.searchTextField.tap()
        self.searchTextField.typeText("NonExistentEnterprise 123")
        
        XCTAssertTrue(self.noResultsLabel.waitForExistence(timeout: 5.0))
    }
    
    func testSearchExistentEnterprise() {
        
        self.testSignInWithCorrectCredentials()
        
        XCTAssertTrue(self.searchTextField.waitForExistence(timeout: 5.0))
        
        self.searchTextField.tap()
        self.searchTextField.typeText("HSQ")
        
        XCTAssertFalse(self.noResultsLabel.waitForExistence(timeout: 5.0))
    }
    
}


