//
//  ioasys_appUnitTests.swift
//  ioasys_appUnitTests
//
//  Created by Marcos Kilmer on 03/01/21.
//

import XCTest
import ioasys_app

//MARK:- Testes Unitários do projeto
class ioasys_appUnitTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testWithEmptyEmailSignIn() throws {
        let wrongEmail = ""
        let wrongPassword = "noEmptyPassword"
        let signInViewModel = SignInViewModel(email: wrongEmail, password: wrongPassword)
        
       XCTAssertFalse(signInViewModel.isValidEmail())
        
    }
    
    func testWithEmptyPasswordSignIn() throws {
        let wrongEmail = "noEmptyEmail"
        let wrongPassword = ""
        let signInViewModel = SignInViewModel(email: wrongEmail, password: wrongPassword)
        
        
       XCTAssertFalse(signInViewModel.isValidPassword())
        
    }

}
